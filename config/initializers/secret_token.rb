# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
EventManagerWebService::Application.config.secret_key_base = '8d133cc8874b0acd2ef250594a6b77948b123d0a316d142e80155c1c9ab34f065fd5047666d0bb962585d6720f4115308cb2d3ab4310920a587b7d5aabb1a58e'
