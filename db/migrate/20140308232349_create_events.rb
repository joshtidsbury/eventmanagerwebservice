class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.datetime :start_date_time
      t.datetime :end_date_time
      t.integer :capacity
      t.string :name
      t.text :description
      t.integer :location_room_id

      t.timestamps
    end
  end
end
