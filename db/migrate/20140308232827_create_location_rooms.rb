class CreateLocationRooms < ActiveRecord::Migration
  def change
    create_table :location_rooms do |t|
      t.string :name
      t.string :description
      t.integer :location_id

      t.timestamps
    end
  end
end
