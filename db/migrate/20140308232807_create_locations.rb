class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :city
      t.string :province
      t.string :postal_code
      t.string :address_one
      t.string :address_two
      t.string :name
      t.text :description
      t.string :country

      t.timestamps
    end
  end
end
